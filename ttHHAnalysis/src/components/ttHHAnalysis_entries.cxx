#include "../BaselineVarsttHHAlg.h"
#include "../JetPairingAlgttHH.h"
#include "../SelectionFlagsttHH.h"

using namespace ttHH;

DECLARE_COMPONENT(BaselineVarsttHHAlg)
DECLARE_COMPONENT(JetPairingAlgttHH)
DECLARE_COMPONENT(SelectionFlagsttHHAlg)
